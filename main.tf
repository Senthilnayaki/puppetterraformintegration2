provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "example" {
  ami           = "ami-0d6f74b9139d26bf1"
  instance_type = "t2.micro"
  key_name      = "mykey1"
  tags = {
    Name = "senthil1-instance"
  }
  connection {
    type     = "ssh"
    user     = "ec2-user"  
    private_key = file("${path.module}/mykey1.pem") 
    host        = self.public_ip   
  }
  provisioner "remote-exec" {
  inline = [
    "sudo yum install -y puppet",
    "sudo puppet apply /etc/puppet/site.pp" ,
    "sudo puppet agent --test"
  ]
  }
}
